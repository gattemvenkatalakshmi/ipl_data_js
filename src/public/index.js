const fs = require("fs");
const csv = require("csv-parser");
const deliveries = [];
const matches = [];
matchesfilepath = "../data/matches.csv";
deliveriesfilepath = "../data/deliveries.csv";
function readCSV(filePath, data, jsonpath) {
  // Check if the file has a .csv extension
  if (filePath && filePath.endsWith(".csv")) {
    // Create a readable stream for the CSV file
    const readStream = fs.createReadStream(filePath);

    // Use csv-parser to parse the CSV file stream
    readStream
      .pipe(csv())
      .on("data", (row) => {
        data.push(row);
      })
      .on("end", () => {
        console.log(`Data from ${filePath} has been loaded.`);
        // Write the data to a JSON file
        writeToJsonFile(data,jsonpath);
        //console.log(data);
      })
      .on("error", (err) => {
        console.error(`Error reading ${filePath}: ${err.message}`);
      });
  } else {
    console.error(`Error: ${filePath} is not a CSV file.`);
  }
}
readCSV(matchesfilepath, matches,'matches.json');
readCSV(deliveriesfilepath, deliveries ,'deliveries.json');
function writeToJsonFile(data, path) {
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync(path, jsonData);
  console.log("Data has been written to output.json");
}
