const fs = require("fs");
const jsonFilePath = "../public/deliveries.json";
const outputFilePath =
  "../public/output/8-highest-no-times-player-dismissed.json";
function readDataFromJson(jsonFilePath) {
  let jsonData = [];

  try {
    // Read file content synchronously
    const fileContent = fs.readFileSync(jsonFilePath, "utf8");
    // Parse JSON content
    jsonData = JSON.parse(fileContent);
  } catch (error) {
    console.error("Error reading or parsing JSON file:", error);
  }
  return jsonData;
}
function highestTimesPlayerDismissed(deliveriesData) {
  let resultedHighest;
  if (Array.isArray(deliveriesData)) {
    const highestPlayerDismissed = deliveriesData
      .map((matchesRow) => {
        return {
          batsman: matchesRow.batsman,
          bowler: matchesRow.bowler,
          dismissal_kind: matchesRow.dismissal_kind,
        };
      })
      .filter(
        (match) =>
          match.dismissal_kind !== "run out" && match.dismissal_kind !== ""
      )

      .reduce(function (dismissedPlayerObj, seasonwinners) {
        let batsman = seasonwinners.batsman;
        let bowler = seasonwinners.bowler;
        if (dismissedPlayerObj.hasOwnProperty(batsman)) {
          let player = dismissedPlayerObj[batsman];
          if (player.hasOwnProperty(bowler)) {
            player[bowler] = player[bowler] + 1;
          } else {
            player[bowler] = 1;
          }
        } else {
          dismissedPlayerObj[batsman] = {};
          dismissedPlayerObj[batsman][bowler] = 1;
        }
        return dismissedPlayerObj;
      }, {});
    let highesttimes = 0;
    let highestBatsman;
    let dismissedplayer;
    let value = false;
    for (const player in highestPlayerDismissed) {
      let bastman = highestPlayerDismissed[player];

      for (const bowler in bastman) {
        if (bastman[bowler] > highesttimes) {
          highesttimes = bastman[bowler];
          dismissedplayer = bowler;
          value = true;
        }
      }
      if (value) {
        value = false;
        highestBatsman = player;
      }
    }
    let bowlerTimes = {};
    bowlerTimes[dismissedplayer] = highesttimes;
    resultedHighest = {};
    resultedHighest[highestBatsman] = bowlerTimes;
  }
  return resultedHighest;
}
function writeToJsonFile(data, outputFilePath) {
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync(outputFilePath, jsonData);
  console.log("Data has been written to output.json");
}
writeToJsonFile(
  highestTimesPlayerDismissed(readDataFromJson(jsonFilePath)),
  outputFilePath
);
