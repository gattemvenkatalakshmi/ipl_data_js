const fs = require("fs");
const jsonFilePath = "../public/matches.json";
const outputFilePath = "../public/output/5-toss-winner-and-winner.json";
function readDataFromJson(jsonFilePath) {
  let jsonData = [];

  try {
    // Read file content synchronously
    const fileContent = fs.readFileSync(jsonFilePath, "utf8");
    // Parse JSON content
    jsonData = JSON.parse(fileContent);
    // console.log(matchId2015);
  } catch (error) {
    console.error("Error reading or parsing JSON file:", error);
  }
  return jsonData;
}

function tossWinnerAndWinner(matchesData) {
  //console.log(matchesData);
  let tossWinners;
  if (Array.isArray(matchesData)) {
    tossWinners = matchesData
      .map((matches) => {
        return { toss_winner: matches.toss_winner, winner: matches.winner };
      })
      .filter((matches) => matches.toss_winner == matches.winner)
      .reduce(function (TeamsObj, team) {
        let team1 = team.toss_winner;
        if (TeamsObj.hasOwnProperty(team1)) {
          TeamsObj[team1] = TeamsObj[team1] + 1;
        } else {
          TeamsObj[team1] = 1;
        }
        return TeamsObj;
      }, {});
  }
  return tossWinners;
}
function writeToJsonFile(data, outputFilePath) {
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync(outputFilePath, jsonData);
  console.log("Data has been written to output.json");
}
writeToJsonFile(
  tossWinnerAndWinner(readDataFromJson(jsonFilePath)),
  outputFilePath
);
