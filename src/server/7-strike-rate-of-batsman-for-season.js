const { forEach } = require("async");
const fs = require("fs");
const jsonFilePath = "../public/matches.json";
const deliveryJsonFilePath = "../public/deliveries.json";
const outputFilePath = "../public/output/7-strike-rate-ofbatsman.json";
function readDataFromJson(deliveryJsonFilePath) {
  let jsonData2 = [];

  try {
    // Read file content synchronously

    const fileContent2 = fs.readFileSync(deliveryJsonFilePath, "utf8");
    // Parse JSON content

    jsonData2 = JSON.parse(fileContent2);
  } catch (error) {
    console.error("Error reading or parsing JSON file:", error);
  }
  const result = economicalBowlers(jsonData2);
  return result;
}
function economicalBowlers(deliveriesData) {
  let bowlersecomy;
  if (Array.isArray(deliveriesData)) {
    bowlersecomy = deliveriesData
      .filter((deliveries) => deliveries.total_runs != "0")
      .map(function (deliveries) {
        return {
          batsman: deliveries.batsman,
          total_runs: deliveries.total_runs,
          season: deliveries.season,
        };
      })
      .reduce(function (batsmanObj, deliveries) {
        let batsman = deliveries.batsman;
        if (batsmanObj.hasOwnProperty(batsman)) {
          batsmanObj[batsman].total_runs += Number(deliveries.total_runs);
          batsmanObj[batsman].balls += 1;
        } else {
          batsmanObj[batsman] = {
            total_runs: Number(deliveries.total_runs),
            balls: 1,
          };
        }
        return batsmanObj;
      }, {});
    for (const bowlers in bowlersecomy) {
      bowlersecomy[bowlers] =
        (bowlersecomy[bowlers].total_runs / bowlersecomy[bowlers].balls) * 100;
    }
  }
  return bowlersecomy;
}
function writeToJsonFile(data, outputFilePath) {
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync(outputFilePath, jsonData);
  console.log("Data has been written to output.json");
}
writeToJsonFile(readDataFromJson(deliveryJsonFilePath), outputFilePath);
