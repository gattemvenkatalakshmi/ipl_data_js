const fs = require("fs");
const jsonFilePath = "../public/matches.json";
const outputFilePath = "../public/output/2-matches-won-per-season.json";
function readDataFromJson(jsonFilePath) {
  let jsonData = [];

  try {
    // Read file content synchronously
    const fileContent = fs.readFileSync(jsonFilePath, "utf8");
    // Parse JSON content
    jsonData = JSON.parse(fileContent);
  } catch (error) {
    console.error("Error reading or parsing JSON file:", error);
  }
  return jsonData;
}
function matchesWonPerTeam(matchesData) {
  let matchesAndSeasons;
  if (Array.isArray(matchesData)) {
    matchesAndSeasons = matchesData
      .map((matchesRow) => {
        return { season: matchesRow.season, winner: matchesRow.winner };
      })
      .filter((match) => match.season !== "" && match.winner !== "")

      .reduce(function (winnerYearsobj, seasonwinners) {
        let year = seasonwinners.season;
        let team = seasonwinners.winner;
        if (winnerYearsobj.hasOwnProperty(year)) {
          let yearTeam = winnerYearsobj[year];
          if (yearTeam.hasOwnProperty(team)) {
            yearTeam[team] = yearTeam[team] + 1;
          } else {
            yearTeam[team] = 1;
          }
        } else {
          winnerYearsobj[year] = {};
          winnerYearsobj[year][team] = 1;
        }
        return winnerYearsobj;
      }, {});
    // .reduce((winnerYearsobj, { season, winner }) => {
    //     winnerYearsobj[season] = { ...winnerYearsobj[season], [winner]: (winnerYearsobj[season]?.[winner] || 0) + 1 } || { [winner]: 1 };
    //     return winnerYearsobj;
    //   }, {});
    return matchesAndSeasons;
  }
}
function writeToJsonFile(data, outputFilePath) {
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync(outputFilePath, jsonData);
  console.log("Data has been written to output.json");
}
writeToJsonFile(
  matchesWonPerTeam(readDataFromJson(jsonFilePath)),
  outputFilePath
);
