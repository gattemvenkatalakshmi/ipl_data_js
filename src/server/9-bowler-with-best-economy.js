const { forEach } = require("async");
const fs = require("fs");
const jsonFilePath = "../public/matches.json";
const deliveryJsonFilePath = "../public/deliveries.json";
const outputFilePath = "../public/output/9-best-bowler-eomy.json";
function readDataFromJson(deliveryJsonFilePath) {
  let jsonData2 = [];

  try {
    // Read file content synchronously

    const fileContent2 = fs.readFileSync(deliveryJsonFilePath, "utf8");
    // Parse JSON content

    jsonData2 = JSON.parse(fileContent2);
  } catch (error) {
    console.error("Error reading or parsing JSON file:", error);
  }
  const result = economicalBowlers(jsonData2);
  return result;
}
function economicalBowlers(deliveriesData) {
  let bestBowlerEcomy = {};
  if (Array.isArray(deliveriesData)) {
    const bowlersecomy = deliveriesData
      .filter(
        (deliveries) =>
          deliveries.is_super_over == 1 && deliveries.total_runs != "0"
      )
      .map(function (deliveries) {
        return { bowler: deliveries.bowler, total_runs: deliveries.total_runs };
      })
      .reduce(function (bowlersObj, deliveries) {
        let bowler = deliveries.bowler;
        if (bowlersObj.hasOwnProperty(bowler)) {
          bowlersObj[bowler].total_runs += Number(deliveries.total_runs);
          bowlersObj[bowler].balls += 1;
        } else {
          bowlersObj[bowler] = {
            total_runs: Number(deliveries.total_runs),
            balls: 1,
          };
        }
        return bowlersObj;
      }, {});
    for (const bowlers in bowlersecomy) {
      bowlersecomy[bowlers] =
        (bowlersecomy[bowlers].total_runs / bowlersecomy[bowlers].balls) * 6;
    }
    let lowest = Number.MAX_VALUE;
    let bestbowler;
    for (const bowlers in bowlersecomy) {
      if (bowlersecomy[bowlers] < lowest) {
        lowest = bowlersecomy[bowlers];
        bestbowler = bowlers;
      }
    }

    bestBowlerEcomy[bestbowler] = lowest;
  }
  return bestBowlerEcomy;
}
function writeToJsonFile(data, outputFilePath) {
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync(outputFilePath, jsonData);
  console.log("Data has been written to output.json");
}
writeToJsonFile(readDataFromJson(deliveryJsonFilePath), outputFilePath);
