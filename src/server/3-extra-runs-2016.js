const { forEach } = require("async");
const fs = require("fs");
const jsonFilePath = "../public/matches.json";
const deliveryJsonFilePath = "../public/deliveries.json";
const outputFilePath = "../public/output/3-extra-runs-2016.json";
function readDataFromJson(jsonFilePath, deliveryJsonFilePath) {
  let jsonData = [];
  let jsonData2 = [];

  try {
    // Read file content synchronously
    const fileContent = fs.readFileSync(jsonFilePath, "utf8");
    const fileContent2 = fs.readFileSync(deliveryJsonFilePath, "utf8");
    // Parse JSON content
    jsonData = JSON.parse(fileContent);
    jsonData2 = JSON.parse(fileContent2);

    // console.log(matchId2015);
  } catch (error) {
    console.error("Error reading or parsing JSON file:", error);
  }
  const result = extraRunsConceded2016(jsonData, jsonData2);
  return result;
}
function extraRunsConceded2016(matchesData, deliveriesData) {
  let extraRunsConcededPerTeam;
  if (Array.isArray(matchesData) && Array.isArray(deliveriesData)) {
    let matchId_2016 = getMatchId(matchesData);

    extraRunsConcededPerTeam = deliveriesData
      .filter(function (deliveries) {
        if (deliveries.match_id in matchId_2016) {
          return deliveries;
        }
      })
      .map(function (deliveries) {
        return {
          bowling_team: deliveries.bowling_team,
          extra_runs: deliveries.extra_runs,
        };
      })
      .reduce(function (extraRunsObj, deliveries) {
        let team = deliveries.bowling_team;
        if (extraRunsObj.hasOwnProperty(team)) {
          extraRunsObj[team] =
            extraRunsObj[team] + Number(deliveries.extra_runs);
        } else {
          extraRunsObj[team] = Number(deliveries.extra_runs);
        }

        return extraRunsObj;
      }, {});
  }
  return extraRunsConcededPerTeam;
}
function getMatchId(matchesData) {
  let matchesId = {};
  matchesData.forEach(function getID(matches) {
    if (matches.season == 2016) {
      matchesId[matches.id] = matches.season;
    }
  });
  return matchesId;
}
function writeToJsonFile(data, outputFilePath) {
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync(outputFilePath, jsonData);
  console.log("Data has been written to output.json");
}
writeToJsonFile(
  readDataFromJson(jsonFilePath, deliveryJsonFilePath),
  outputFilePath
);
