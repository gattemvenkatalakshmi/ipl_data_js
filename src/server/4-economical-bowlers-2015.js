const { forEach } = require("async");
const fs = require("fs");
const jsonFilePath = "../public/matches.json";
const deliveryJsonFilePath = "../public/deliveries.json";
const outputFilePath = "../public/output/4-top10-economical-bowlers-2015.json";
function readDataFromJson(jsonFilePath, deliveryJsonFilePath) {
  let jsonData = [];
  let jsonData2 = [];

  try {
    // Read file content synchronously
    const fileContent = fs.readFileSync(jsonFilePath, "utf8");
    const fileContent2 = fs.readFileSync(deliveryJsonFilePath, "utf8");
    // Parse JSON content
    jsonData = JSON.parse(fileContent);
    jsonData2 = JSON.parse(fileContent2);

    // console.log(matchId2015);
  } catch (error) {
    console.error("Error reading or parsing JSON file:", error);
  }
  return economicalBowlers2015(jsonData, jsonData2);
}
function economicalBowlers2015(matchesData, deliveriesData) {
  let top10BowlersEco;
  if (Array.isArray(matchesData) && Array.isArray(deliveriesData)) {
    let matchId_2015 = getMatchId(matchesData);
    const bowlersecomy = deliveriesData
      .filter(
        (deliveries) =>
          deliveries.match_id in matchId_2015 && deliveries.total_runs != "0"
      )
      .map(function (deliveries) {
        return { bowler: deliveries.bowler, total_runs: deliveries.total_runs };
      })
      .reduce(function (bowlersObj, deliveries) {
        let bowler = deliveries.bowler;
        if (bowlersObj.hasOwnProperty(bowler)) {
          bowlersObj[bowler].total_runs += Number(deliveries.total_runs);
          bowlersObj[bowler].balls += 1;
        } else {
          bowlersObj[bowler] = {
            total_runs: Number(deliveries.total_runs),
            balls: 1,
          };
        }
        return bowlersObj;
      }, {});
    top10BowlersEco = toGetHighest(bowlersecomy);
  }
  return top10BowlersEco;
}
function toGetHighest(bowlersData) {
  for (const bowlers in bowlersData) {
    bowlersData[bowlers] =
      (bowlersData[bowlers].total_runs / bowlersData[bowlers].balls) * 6;
  }
  const sortable = Object.fromEntries(
    Object.entries(bowlersData).sort(([, a], [, b]) => a - b)
  );
  let to10EcoBowlers = Object.fromEntries(
    Object.entries(sortable).slice(0, 10)
  );

  return to10EcoBowlers;
}
function getMatchId(matchesData) {
  let matchesId = {};
  matchesData.forEach(function getID(matches) {
    if (matches.season == 2015) {
      matchesId[matches.id] = matches.season;
    }
  });
  return matchesId;
}
function writeToJsonFile(data, outputFilePath) {
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync(outputFilePath, jsonData);
  console.log("Data has been written to output.json");
}
writeToJsonFile(
  readDataFromJson(jsonFilePath, deliveryJsonFilePath),
  outputFilePath
);
