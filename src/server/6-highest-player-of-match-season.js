const fs = require("fs");
const jsonFilePath = "../public/matches.json";
const outputFilePath = "../public/output/6-highest-player-season.json";
function readDataFromJson(jsonFilePath) {
  let jsonData = [];

  try {
    // Read file content synchronously
    const fileContent = fs.readFileSync(jsonFilePath, "utf8");
    // Parse JSON content
    jsonData = JSON.parse(fileContent);
  } catch (error) {
    console.error("Error reading or parsing JSON file:", error);
  }
  return jsonData;
}
function highestPlayerOfMatch(matchesData) {
  let highestplayer = {};
  if (Array.isArray(matchesData)) {
    const seasonAndPlayer = matchesData
      .map((matchesRow) => {
        return {
          season: matchesRow.season,
          player_of_match: matchesRow.player_of_match,
        };
      })
      .filter((match) => match.season !== "" && match.winner !== "")

      .reduce(function (winnerYearsobj, seasonwinners) {
        let year = seasonwinners.season;
        let player_of_match = seasonwinners.player_of_match;
        if (winnerYearsobj.hasOwnProperty(year)) {
          let yearplayer_of_match = winnerYearsobj[year];
          if (yearplayer_of_match.hasOwnProperty(player_of_match)) {
            yearplayer_of_match[player_of_match] =
              yearplayer_of_match[player_of_match] + 1;
          } else {
            yearplayer_of_match[player_of_match] = 1;
          }
        } else {
          winnerYearsobj[year] = {};
          winnerYearsobj[year][player_of_match] = 1;
        }

        return winnerYearsobj;
      }, {});

    for (const years in seasonAndPlayer) {
      let season = seasonAndPlayer[years];
      let highest = 0;
      let highPlayer;
      for (const player in season) {
        if (season[player] > highest) {
          highest = season[player];
          highPlayer = player;
        }
      }
      let yearwise = {};
      yearwise[highPlayer] = highest;
      highestplayer[years] = yearwise;
    }
  }
  return highestplayer;
}
function writeToJsonFile(data, outputFilePath) {
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync(outputFilePath, jsonData);
  console.log("Data has been written to output.json");
}
writeToJsonFile(
  highestPlayerOfMatch(readDataFromJson(jsonFilePath)),
  outputFilePath
);
