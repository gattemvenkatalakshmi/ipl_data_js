const fs = require("fs");
const jsonFilePath = "../public/matches.json";
const outputFilePath = "../public/output/1-matches-per-season.json";
function readDataFromJson(jsonFilePath) {
  let jsonData = [];

  try {
    // Read file content synchronously
    const fileContent = fs.readFileSync(jsonFilePath, "utf8");
    // Parse JSON content
    jsonData = JSON.parse(fileContent);
    // console.log(matchId2015);
  } catch (error) {
    console.error("Error reading or parsing JSON file:", error);
  }
  return jsonData;
}
matchesPerYear(readDataFromJson(jsonFilePath));
function matchesPerYear(matchesData) {
  let matchesYears;
  //console.log(matchesData);
  if (Array.isArray(matchesData)) {
    matchesYears = matchesData
      .map((matches) => {
        return matches.season;
      })
      .reduce(function (Yearsobj, year) {
        if (Yearsobj.hasOwnProperty(year)) {
          Yearsobj[year] = Yearsobj[year] + 1;
        } else {
          Yearsobj[year] = 1;
        }
        return Yearsobj;
      }, {});
  }
  return matchesYears;
}
function writeToJsonFile(data, outputFilePath) {
  const jsonData = JSON.stringify(data, null, 2);
  fs.writeFileSync(outputFilePath, jsonData);
  console.log("Data has been written to output.json");
}
writeToJsonFile(matchesPerYear(readDataFromJson(jsonFilePath)), outputFilePath);
